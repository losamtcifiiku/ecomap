package eco.map.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import eco.map.databinding.MainFragmentBinding
import org.oscim.layers.tile.buildings.BuildingLayer
import org.oscim.layers.tile.vector.VectorTileLayer
import org.oscim.layers.tile.vector.labeling.LabelLayer
import org.oscim.theme.VtmThemes
import org.oscim.tiling.source.mapfile.MapFileTileSource

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)

        @Suppress("UNREACHABLE_CODE")
        binding.map.map()?.let { map ->
            MapFileTileSource().apply {
                TODO("determine where do we store maps")
                // setMapFile("/path/to/map")
                // setMapFileInputStream(stream: FileInputStream)
            }.let { tileSource ->
                val tileLayer = VectorTileLayer(map, tileSource)
                map.layers().apply {
                    add(BuildingLayer(map, tileLayer))
                    add(LabelLayer(map, tileLayer))
                }
            }

            map.setTheme(VtmThemes.DEFAULT)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }
}